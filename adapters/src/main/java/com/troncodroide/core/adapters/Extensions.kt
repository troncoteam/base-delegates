package com.troncodroide.core.adapters

import android.content.Context
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.troncodroide.app.scrmlidl.ui.models.TextColor
import com.troncodroide.app.scrmlidl.ui.models.TextSize
import com.troncodroide.app.scrmlidl.ui.models.TextUi

fun View.visible(visible: Boolean) {
    visibility = if (visible) View.VISIBLE else View.GONE
}

fun Context.toast(message: String) = Toast.makeText(this, message, Toast.LENGTH_LONG).show()

fun TextView.apply(textUi: TextUi?) {

    visible(textUi != null && textUi.text.isNotEmpty())
    if (visibility == View.GONE) return
    textUi?.let {

        this.text = textUi.text
        this.setTextColor(ContextCompat.getColor(context, when (textUi.textColor) {
            TextColor.PRIMARY -> R.color.textPrimary
            TextColor.SECONDARY -> R.color.textSecondary
            TextColor.ACCENT -> R.color.textAccent
        }))
        /*this.textSize = resources.getDimension(when (textUi.textSize) {
            TextSize.L -> R.dimen.text_l
            TextSize.M -> R.dimen.text_m
            TextSize.S -> R.dimen.text_s
            TextSize.XXL -> R.dimen.text_xxl
            TextSize.XL -> R.dimen.text_xl
            TextSize.XS -> R.dimen.text_xs
            TextSize.XXS -> R.dimen.text_xxs
        })*/
        this.textSize = when (textUi.textSize) {
            TextSize.XXL -> 22f
            TextSize.XL -> 20f
            TextSize.L -> 18f
            TextSize.M -> 17f
            TextSize.S -> 16f
            TextSize.XS -> 14f
            TextSize.XXS -> 12f
        }
    }
}