package com.troncodroide.core.adapters.models

data class SwitchItemUI(
    override val estableId: Long,
    val couponID: String,
    val title: TextUi,
    val subtitle: TextUi,
    val selected: Boolean
) : EstableItem(estableId)
