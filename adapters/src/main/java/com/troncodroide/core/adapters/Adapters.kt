package com.troncodroide.core.adapters

import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegate
import com.troncodroide.core.adapters.models.EstableItem
import com.troncodroide.core.adapters.models.SwitchItemUI
import com.troncodroide.core.adapters.models.TextIconItemUI
import com.troncodroide.core.adapters.models.ThreeLinesItemUI
import com.troncodroide.core.adapters.models.TwoLinesItemUI
import kotlinx.android.synthetic.main.view_icon_text_item.view.*
import kotlinx.android.synthetic.main.view_switch_item.view.*
import kotlinx.android.synthetic.main.view_three_lines_item.view.*
import kotlinx.android.synthetic.main.view_two_lines_item.view.*

fun twoLinesAdapterDelegate() =
    adapterDelegate<TwoLinesItemUI, EstableItem>(R.layout.view_two_lines_item) {
        bind { payload ->
            with(item) {
                itemView.view_two_lines_item_title.apply(title)
                itemView.view_two_lines_item_subtitle.apply(subtitle)
            }
        }
    }

fun threeLinesAdapterDelegate() =
    adapterDelegate<ThreeLinesItemUI, EstableItem>(R.layout.view_three_lines_item) {
        bind { payload ->
            with(item) {
                itemView.view_three_lines_item_category.apply(category)
                itemView.view_three_lines_item_title.apply(title)
                itemView.view_three_lines_item_subtitle.apply(subtitle)
            }
        }
    }

fun textIconAdapterDelegate() =
    adapterDelegate<TextIconItemUI, EstableItem>(R.layout.view_icon_text_item) {
        bind { payload ->
            with(item) {
                itemView.view_icon_text_item_text.apply(text)
                itemView.view_icon_text_item_icon.setImageResource(icon)
            }
        }
    }

fun switchAdapterDelegate(activeChanged: (couponId: String, enabled: Boolean) -> Unit) =
    adapterDelegate<SwitchItemUI, EstableItem>(R.layout.view_switch_item) {
        bind { payload ->
            var notifySwitchActions = true
            itemView.view_switch_item_switch.setOnCheckedChangeListener { buttonView, isChecked ->
                if (notifySwitchActions) activeChanged(item.couponID, isChecked)
            }
            with(item) {
                notifySwitchActions = false
                itemView.view_switch_item_switch.isChecked = selected
                notifySwitchActions = true
                itemView.view_switch_item_title.apply(title)
                itemView.view_switch_item_subtitle.apply(subtitle)
            }
        }
    }