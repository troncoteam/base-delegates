package com.troncodroide.core.adapters.models

data class TextUi(val text: String, val textSize: TextSize, val textColor: TextColor) {
    companion object {
        fun default(text: String): TextUi = TextUi(text, TextSize.S, TextColor.PRIMARY)
        fun title(text: String): TextUi = TextUi(text, TextSize.M, TextColor.PRIMARY)
        fun secondary(text: String): TextUi = TextUi(text, TextSize.S, TextColor.SECONDARY)
        fun accent(text: String): TextUi = TextUi(text, TextSize.S, TextColor.ACCENT)
        fun empty(): TextUi = TextUi("", TextSize.S, TextColor.PRIMARY)
    }
}

enum class TextSize {
    XXL, XL, L, M, S, XS, XXS
}

enum class TextColor {
    PRIMARY, SECONDARY, ACCENT
}