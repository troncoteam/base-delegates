package com.troncodroide.core.adapters.models

data class ThreeLinesItemUI(
    override val estableId: Long,
    val couponID: String,
    val title: TextUi,
    val subtitle: TextUi,
    val category: TextUi
) : EstableItem(estableId)
