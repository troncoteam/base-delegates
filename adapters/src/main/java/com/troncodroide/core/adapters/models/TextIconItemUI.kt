package com.troncodroide.core.adapters.models

import androidx.annotation.DrawableRes

data class TextIconItemUI(
    override val estableId: Long,
    val text: TextUi,
    @DrawableRes val icon: Int
) : EstableItem(estableId)
